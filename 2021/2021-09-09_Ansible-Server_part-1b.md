# 2021 Twitch Streaming Notes: September 9, 2021

**Note:** This stream did not occur.

## Topic

Build Ansible Server on Rapsberry Pi 2: Part 1b: The Do-Over

## Pre-Stream Notes

- [x] Fresh install of Raspberry Pi OS Lite 2021-05-07
- [x] ssh enabled

## Today's Goals

- [ ] Change default password
- [ ] Run updates
- [ ] Modify bootstrap script
  - [ ] Download Ansible project
  - [ ] Install Docker
  - [ ] Install Ansible in Docker
- [ ] Start on rewriting entry point script in Python

## Post-Stream Notes

- [ ] None
