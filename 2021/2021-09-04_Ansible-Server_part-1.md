# 2021 Twitch Streaming Notes: September 4, 2021

## Topic

Build Ansible Server on Rapsberry Pi 2: Part 1

## Pre-Stream Notes

These steps were performed before the stream started.

- [x] Raspberry Pi OS Lite 2021-05-07 installed
- [x] ssh enabled
- [x] Static IP address added to router
- [x] DHCP address added to router
- [x] Changed Password
- [x] Run Updates

## Today's Goals

- [x] Bootstrap Script
  - [x] Hello World
  - [x] Aptitude installed
  - [x] Swap Space
  - [x] Python Virtual Environment
  - [x] Install Ansible

Ansible installation itself was taking awhile, so ended stream while this step was occurring.

## Post-Stream Notes

* The Ansible installation steps were failing due to permissions issues.
  * The error message was suggesting to use "--user" on the pip command.
  * This appeared to be working, but after it completed, Ansible was not installed in the Python virtual environment we created for it.
  * Learned that adding "--user" to the pip command was forcing pip to install under /home/pi/.local/lib/python3.7/...
  * Finally realized the permissions problem was caused by "sudo" being in front of the command that creates the Python virtual environment, so it was actually created with the root user.
  * Cleaned up the file system, re-ran the bootstrap script without sudo, installation completed.
  * Each test run was taking between 10 - 15 minutes.
 
Once Ansible was installed, I issued the version command and saw this message:

```text
(ansible-env) pi@raspberrypi:/work/py3-envs/ansible-env $ ansible --version
[DEPRECATION WARNING]:  Ansible will require Python 3.8 or newer on the controller starting with 
Ansible 2.12. Current version: 3.7.3 (default, Jan 22 2021, 20:04:44) [GCC 8.3.0]. This feature 
will be removed from ansible-core in version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.
ansible [core 2.11.4] 
  config file = None
  configured module search path = ['/home/pi/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /work/py3-envs/ansible-env/lib/python3.7/site-packages/ansible
  ansible collection location = /home/pi/.ansible/collections:/usr/share/ansible/collections
  executable location = /work/py3-envs/ansible-env/bin/ansible
  python version = 3.7.3 (default, Jan 22 2021, 20:04:44) [GCC 8.3.0]
  jinja version = 3.0.1
  libyaml = False
```

Two options can fix this problem:

  1. Install a newer version of Python on the Raspberry Pi (see below).
  1. Install Docker and containerize the Ansible Server.
     * This is exactly the type of scenario Docker was made to solve.
     * This is how I have always run Ansible in the past, but it adds a layer of complexity.  I was trying to install Ansible natively to make it simpler.

I decided to see how much work it would be to install a newer version of Python on the Raspberry pi.  I found these instructions [here](https://raspberrytips.com/install-latest-python-raspberry-pi/).

```bash
cd /tmp2
curl --output Python-3.9.7.tgz https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tgz
tar -zxvf Python-3.9.7.tgz
cd Python-3.9.7

# This command took a few minutes
./configure --enable-optimizations

# This command took about 20 minutes
sudo make altinstall
```

Installing the newer version of Python worked without issue.

I am going to re-think how I want to handle this project.  Once a decision is made, I will stream part 2.
