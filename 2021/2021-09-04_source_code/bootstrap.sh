#!/bin/bash
set -e

ANSIBLE_ENV="ansible-env"
PYTHON_ENV_BASE_DIR="/work/py3-envs"


# Main processing logic
function main() {
    adjust_swapfile_size
    setup_server_dirs
    install_apt_tools
    setup_python_virtual_env
    install_ansible
}


function setup_server_dirs() {
    echo "Setting up Ansible Server directories"
    set +e

    sudo mkdir "/work"
    sudo chmod 777 "/work"

    sudo mkdir "/data"
    sudo chmod 777 "/data"

    sudo mkdir "/tmp2"
    sudo chmod 777 "/tmp2"

    sudo mkdir "/work/py3-envs"
    sudo chown pi:pi "/work/py3-envs"
    sudo chmod 755 "/work/py3-envs"

    set -e
}


# Change the Raspberry Pi Swapfile size
function adjust_swapfile_size() {
    echo "Adjusting Raspberry Pi Swapfile Size"
    local rpi_swapfile="/etc/dphys-swapfile"
    sudo dphys-swapfile swapoff
    sudo sed -i 's/^CONF_SWAPSIZE=.*$/CONF_SWAPSIZE=1024/' "${rpi_swapfile}"
    sudo dphys-swapfile setup
    sudo dphys-swapfile swapon   
}


# Install tools via apt
function install_apt_tools() {
    echo "Installing tools from apt"
    sudo apt update
    sudo apt install -y aptitude python3-pip python3-venv
}


# Setup Python virtual environment to install Ansible in to
function setup_python_virtual_env() {
    echo "Setting up Ansible Python virtual environment"
    if [ -d "${PYTHON_ENV_BASE_DIR}/${ANSIBLE_ENV}" ]
    then
        echo "  Environment already exists"
    else
        cd "${PYTHON_ENV_BASE_DIR}"
        python3 -m venv "${ANSIBLE_ENV}"
    fi
}


# Install Ansible and its dependencies
function install_ansible() {
    echo "Installing Ansible"
    sudo apt install -y sshpass apt-utils curl rustc
    cd "${PYTHON_ENV_BASE_DIR}/${ANSIBLE_ENV}"
    source ./bin/activate
    pip install ansible cryptography passlib
    ansible --version

    # PYTHON VERSION WARNING

    # ansible-galaxy collection install ansible.posix
    # ansible-galaxy collection install ansible.utils
    # ansible-galaxy collection install community.docker
    # ansible-galaxy collection install community.general
    # ansible-galaxy collection install community.mongodb
    # ansible-galaxy collection install community.mysql
}


main
