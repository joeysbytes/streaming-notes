#!/bin/bash
set -e

CURRENT_DIR=$(pwd)
BOOTSTRAP_SCRIPT="bootstrap.sh"
REMOTE_DIR="/home/pi"
REMOTE_USER="pi"
REMOVE_SERVER="AnsibleSvr"
CREDENTIALS_FILE="/data/credentials/AnsibleSvr_pi.txt"


# Main processing logic
function main() {
    send_bootstrap
    run_bootstrap
}

# Send the bootstrap script to the Ansible Server
function send_bootstrap() {
    echo "Sending bootstrap script to Ansible Server"
    sshpass -f "${CREDENTIALS_FILE}" \
        scp "${CURRENT_DIR}/${BOOTSTRAP_SCRIPT}" "${REMOTE_USER}@${REMOVE_SERVER}":"${REMOTE_DIR}/"
}


# Run the bootstrap script on the remote Ansible Server
function run_bootstrap() {
    echo "Running bootstrap script on Ansible Server"
    sshpass -f "${CREDENTIALS_FILE}" \
        ssh "${REMOTE_USER}@${REMOVE_SERVER}" "${REMOTE_DIR}/${BOOTSTRAP_SCRIPT}"
}


main
